FROM python:3.11-slim-buster

COPY container_requirements.txt ./
RUN pip install --no-cache-dir -U -r container_requirements.txt

COPY hello.py ./
COPY my_module/ ./my_module

CMD [ "python", "-u", "./hello.py" ]
