"""Test our method for calling an API. Invoke with 'pytes test_hello.py'"""

import sys

# import requests
import responses

sys.path.append("../my_module")
# This line must come after the path change above, so ignore pylint
from check_api import check_api  # pylint: disable=wrong-import-position

print("hello, world!")

# see https://jsonplaceholder.typicode.com/guide/
@responses.activate
def test_check_api_1():
    url = "https://jsonplaceholder.typicode.com/todos/1"
    responses.add(responses.GET, url, json={"error": "not found"}, status=404)

    my_response = check_api(url)
    assert my_response.status_code == 404


@responses.activate
def test_check_api_2():
    url = "https://jsonplaceholder.typicode.com/todos/1"
    ret_json = {
        "userId": 1,
        "id": 1,
        "title": "delectus aut autem",
        "completed": "false",
    }
    responses.add(responses.GET, url, json=ret_json, status=200)

    my_response = check_api(url)
    assert my_response.status_code == 200
