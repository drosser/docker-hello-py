To create the environment

* install pyenv

    `pyenv install 3.11.0`

    `pyenv local 3.11.0`

* create a local venv

    `python -m venv venv`

    `source venv/bin/activate`

    `python -V`

    `# Should return`

    `Python 3.11.0`

* install requirements

    `pip install -r requirements.txt`

* to generate requirements.txt

    `pip freeze > requirements.txt`

* to use pylint

    `pylint <python-module or python-file>`

* to use black (standardizes formatting for python code)

    `black <python-module or python-file>`

* To build the docker image

    `docker build -t <hub-user>/<repo-name>[:<tag>]`

    `docker build -t drosser9/docker-hello-py:<tag> .`

* To send the image to Docker Hub, do something like:

    `docker push drosser9/docker-hello-py:v2.0.0`

* To run the newly created image, something like:

    `docker run drosser9/docker-hello-py:v1.0.0`

* To check the formatting and syntax of compose.yaml

    `python -c 'import yaml, sys; print(yaml.safe_load(sys.stdin))' < compose.yaml`

* To use v2.0.0 or higher you must use "docker-compose up" instead of "docker run"
