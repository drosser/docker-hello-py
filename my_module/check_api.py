import requests
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

def check_api(url):
    """
    Call the API specified by URL
    arguments: url (string)
    returns: the Request instance my_request
    side-effects: sends HTTP status code and JSON object to stdout
    """
    my_request = requests.get(url, timeout=5)
    logger.info("API status code returned: %s", my_request.status_code)
    logger.info("API json: %s", my_request.json())
    return my_request
