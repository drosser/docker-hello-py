"""Sends "Hello, World!" to logging.logger...and other things"""

import sys
import json
import time
import logging

# Below are not part of the Python std lib
import schedule

# local imports
from my_module.check_api import check_api

# Get the Docker-specific secrets directory into our path.
# See https://docs.docker.com/compose/compose-file/#secrets
# Be careful about naming your modules that might conflict with
# Python standard library modules. They come first in the path.
sys.path.append("/run/secrets")

# Should work whether secrets is in working dir or /run/secrets
# This line must come after the path change above, so ignore pylint
import docker_secrets  # pylint: disable=wrong-import-position


# Setup logging
logger = logging.getLogger(__name__)

root_logger = logging.getLogger("")
root_logger.setLevel(logging.INFO)

handler = logging.StreamHandler()
format_string = "%(asctime)s - %(levelname)s - %(name)s - %(message)s"
formatter = logging.Formatter(format_string)

handler.setFormatter(formatter)
root_logger.addHandler(handler)


def say_hello():
    """
    This method literally says hello
    arguments: none
    returns: none
    side-effects: sends "Hello, World!" to stdout
    """
    logger.info("Hello, World!")


def echo_secret():
    """
    Demonstrate how Docker handles run-time secrets
    arguments: none
    returns: none
    side-effects: sends SECRET1 to stdout
    """
    logger.info(docker_secrets.SECRET1)


SETTINGS_FILE = "/watch/hello_settings.json"
FILE_ENCODING = "utf-8"

if __name__ == "__main__":
    say_hello()
    echo_secret()
    with open(SETTINGS_FILE, encoding=FILE_ENCODING) as sfile:
        hello_settings = json.load(sfile)
    logger.info(hello_settings)
    schedule.every().minute.do(check_api, url=hello_settings["url"])
    while True:
        schedule.run_pending()
        time.sleep(1)
